# FirstBloodProject

Advanced Bash-Scripting Guide 
    http://tldp.org/LDP/abs/html/index.html
    
Linux blog: 
    https://www.tecmint.com/
    https://kernelnewbies.org/KernelDevViewpoint
    https://www.linux.com/
    https://www.linuxbabe.com/
    https://linoxide.com/usr-mgmt/simple-examples-to-explain-linux-file-permissions/

IT blogs:
    https://www.whizlabs.com/blog/

Debian admin:
    https://debian-handbook.info/browse/vi-VN/stable/index.html
    
football
    https://www.fullmatchesandshows.com/

Classical guitar class
    https://www.thisisclassicalguitar.com/

Docker
    https://docs.docker.com/

C for Linux 
    https://linuxconfig.org/c-development-on-linux-introduction-i
Devops
    https://www.devopsschool.com/
Java + Tools + Tech: 
    https://www.vogella.com/tutorials/